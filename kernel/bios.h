#ifndef _BIOS_H
#define _BIOS_H

#define IRQ_INTERRUPT 1
#define IRQ_DISPLAY 2
#define IRQ_TIMER 3
#define IRQ_KEYBOARD 4

#define DISK_SECTOR_SIZE 512

struct diskpart {
  unsigned char number;
  unsigned char type;
  unsigned int sectors;
} __attribute__((packed()));

extern void println(char*);
extern int framebuffer_getsize();
extern void framebuffer_set(void*);
extern void framebuffer_blit();
extern void irq_handler(int, void(*));
extern void irq_wait();
extern void transfer_to_irq();

extern int disk_part(struct diskpart*);
extern int disk_repart(struct diskpart*);
extern int disk_read(unsigned char, unsigned int, unsigned char*);
extern int disk_write(unsigned char, unsigned int, unsigned char*);

#endif
