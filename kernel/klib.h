#ifndef _KLIB_H
#define _KLIB_H

#define FS_REQUIRED_SECTORS 32768
#define FS_TYPE 88

#define FBUF_WIDTH 640
#define FBUF_HEIGHT 200
#define FBUF_SIZE FBUF_WIDTH * FBUF_HEIGHT * 4

void fbrenderbitmap8x8(unsigned char *fb, char *bitmap, int dx, int dy) {
  int x,y;
  int set;
  int mask;
  for (x=0; x < 8; x++) {
    for (y=0; y < 8; y++) {
      set = bitmap[x] & 1 << y;
      int px = dx + y;
      int py = dy + x;
      unsigned char col = set ? 0xFF : 0;
      fb[(py * FBUF_WIDTH + px) * 4] = col;
      fb[(py * FBUF_WIDTH + px) * 4 + 1] = col;
      fb[(py * FBUF_WIDTH + px) * 4 + 2] = col;
      fb[(py * FBUF_WIDTH + px) * 4 + 3] = (unsigned char) 0xFF;
    }
  }
}

#endif
