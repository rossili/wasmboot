#include "bios.h"
#include "klib.h"
#include "font8x8/font8x8_basic.h"

void *memset(void *s, unsigned char c, unsigned int l) {
    unsigned char* p = s;
    while (l--)
        *p++ = c;
    return s;
}

void irq_display() {
  framebuffer_blit();
}

struct diskpart *part;
unsigned char framebuffer[FBUF_SIZE];
extern void _start() {
  framebuffer_getsize();
  framebuffer_set(framebuffer);
  memset(framebuffer, 0, FBUF_SIZE);
  for (int i = 3; i < FBUF_SIZE; i += 4)
    *(framebuffer + i) = 255;

  irq_handler(IRQ_DISPLAY, irq_display);

  fbrenderbitmap8x8(framebuffer, font8x8_basic['w'], 0, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['a'], 8, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['s'], 8 * 2, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['m'], 8 * 3, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['b'], 8 * 4, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['o'], 8 * 5, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['o'], 8 * 6, 8);
  fbrenderbitmap8x8(framebuffer, font8x8_basic['t'], 8 * 7, 8);

  int valid;
  part->number = 0;
  while ((valid = disk_part(part) == 0)) {
    if (part->type == FS_TYPE) break;
    if (part->type == 0 && part->sectors >= FS_REQUIRED_SECTORS) {
      part->type = FS_TYPE;
      disk_repart(part);
      break;
    }
    part->number++;
  }
  if (!valid) {
    println("panic! no suitable partition found.");
    return;
  }

  println("Hello, World!");
  transfer_to_irq();
}
