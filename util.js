export const TXTENC = new TextEncoder()
export const TXTDEC = new TextDecoder()

export class DynamicLinker {}

export class Thread {
  constructor(mod, opts={}) {
    if (!opts) opts = {}
    opts.bios ??= false
    this.disk = opts.disk
    delete opts.disk

    if (opts.sab) {
      this.i32a = new Int32Array(opts.sab)
      this.irqlock = new DoubleLock(this.i32a, 0)
    }

    this.worker = new Worker('thread.js', { type: 'module' })
    this.worker.addEventListener('message', this.messaged.bind(this))
    this.worker.postMessage({ type: 'init', mod, mem: opts.mem, sab: opts.sab, bios: opts.bios, canvas: opts.canvas }, [ opts.canvas ])
  }

  async messaged(msg) {
    const type = msg.data.type
    if (type == 'irq_enable') {
      this.enableIRQ(msg.data.irq)
    } else if (type == 'irq') {
      await this.irqlock.checkAsync(1)
      const irq = this.i32a[2]
      switch (irq) {
        case IRQ.DISKPART:
          const part = this.disk.partition(this.i32a[3])
          if (part) {
            this.i32a[4] = part.type
            this.i32a[5] = part.sectors
          } else {
            this.i32a[4] = -1
          }
          break
        case IRQ.DISKREPART:
          this.disk.repartition(this.i32a[3], this.i32a[4], this.i32a[5])
      }
      this.irqlock.validate(1)
    } else console.warn('Unhandled message:', msg)
  }

  enableIRQ(irq) {
    if (irq == IRQ.DISPLAY) {
      const animate = async () => {
        await this.irq(irq)
        requestAnimationFrame(animate)
      }
      animate()
    }
  }

  async irq(irq) {
    await this.irqlock.lockAsync()
    Atomics.store(this.i32a, 2, irq)
    this.irqlock.validate(0)
    await this.irqlock.checkAsync(0)
    this.irqlock.unlock()
  }

  start(func='_start', args=[]) {
    this.worker.postMessage({ type: 'start', func, args })
  }
}

export class ThreadGroup {

}

// https://stackoverflow.com/a/52171480
const cyrb53 = (buf, seed = 0) => {
  let h1 = 0xdeadbeef ^ seed,
    h2 = 0x41c6ce57 ^ seed
  for (let i = 0, ch; i < buf.length; i++) {
    ch = buf[i]
    h1 = Math.imul(h1 ^ ch, 2654435761)
    h2 = Math.imul(h2 ^ ch, 1597334677)
  }

  h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909)
  h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909)

  return (h2>>>0).toString(16).padStart(8,0)+(h1>>>0).toString(16).padStart(8,0)
}

export async function loadBootrom() {
  const f = await fetch('/kernel/bootrom.wasm')
  if (!f.ok) return null

  const src = await f.arrayBuffer()
  const mod = await WebAssembly.compile(src)
  mod.src = src
  return mod
}

export async function verifyBootrom(mod) {
  const integrity = cyrb53(new Uint8Array(mod.src))
  if (localStorage.getItem('_bootrom_integrity')) {
    if (localStorage.getItem('_bootrom_integrity') != integrity) {
      alert('bootROM verification failed!')
      throw 'bootROM verification failed!'
    }
  } else {
    localStorage.setItem('_bootrom_integrity', integrity)
  }
}

export function readcstr(buf, ptr) {
  const ui8a = new Uint8Array(buf)
  let end = ptr
  while (ui8a[end] != 0) end++

  return TXTDEC.decode(buf.slice(ptr, end))
}

export const IRQ = {
  INTERRUPT: 1,
  DISPLAY: 2,
  TIMER: 3,
  KEYBOARD: 4,

  DISKPART: 5,
  DISKREPART: 6,
  DISKREAD: 7,
  DISKWRITE: 8,
}

async function wrapAtwaAs(a) {
  return a.value
}

export class Lock {
  constructor(i32a, index) {
    this.i32a = i32a
    this.index = index
    Atomics.store(this.i32a, this.index, 0)
  }

  lock() {
    Atomics.wait(this.i32a, this.index, 1)
    const v = Atomics.compareExchange(this.i32a, this.index, 0, 1)
    if (v != 0)
      return this.lock()
  }

  async lockAsync() {
    await wrapAtwaAs(Atomics.waitAsync(this.i32a, this.index, 1))
    const v = Atomics.compareExchange(this.i32a, this.index, 0, 1)
    if (v != 0)
      return await this.lockAsync()
  }

  unlock() {
    Atomics.store(this.i32a, this.index, 0)
    return Atomics.notify(this.i32a, this.index)
  }
}

export class DoubleLock {
  constructor(i32a, index) {
    this.i32a = i32a
    this.index = index
    Atomics.store(this.i32a, this.index, 0)
    Atomics.store(this.i32a, this.index + 1, 1)
  }

  lock() {
    Atomics.wait(this.i32a, this.index, 1)
    const v = Atomics.compareExchange(this.i32a, this.index, 0, 1)
    if (v != 0)
      return this.lock()
    Atomics.store(this.i32a, this.index + 1, 1)
  }

  async lockAsync() {
    await wrapAtwaAs(Atomics.waitAsync(this.i32a, this.index, 1))
    const v = Atomics.compareExchange(this.i32a, this.index, 0, 1)
    if (v != 0)
      return await this.lockAsync()
    Atomics.store(this.i32a, this.index + 1, 1)
  }


  check(v=1) {
    return Atomics.wait(this.i32a, this.index + 1, v)
  }

  async checkAsync(v=1) {
    return wrapAtwaAs(Atomics.waitAsync(this.i32a, this.index + 1, v))
  }

  validate(v=0) {
    Atomics.store(this.i32a, this.index + 1, v)
    return Atomics.notify(this.i32a, this.index + 1)
  }

  unlock() {
    Atomics.store(this.i32a, this.index, 0)
    Atomics.store(this.i32a, this.index + 1, 1)
    return Atomics.notify(this.i32a, this.index)
  }
}

export class Future {
  constructor() {
    let store = {}
    this.promise = new Promise((res, rej) => {
      store.now = x => res(x)
      store.never = x => rej(x)
    })
    this._store = store
  }

  now(x) {
    return this._store.now(x)
  }
  never(x) {
    return this._store.never(x)
  }
}

export class TrapError extends Error {
  constructor(msg) {
    super(msg)
    this.name = 'TrapError'
  }
}

export class PileCache {
  constructor(elements) {
    this.elements = elements
  }
}

export class Disk {
  static SECTOR_SIZE = 512 // bytes
  static BLOCK_SIZE = 1024 // sectors

  constructor(partitions) {
    this.partitions = partitions
    this.dbs = this.partitions.map((_, i) => new Dexie(`wasmboot-diskpart-${i}`))
    this.caches = this.partitions.map(() => new PileCache(128))
  }

  async init() {
    let i = 0
    for (const db of this.dbs) {
      db.version(4).stores({
        blocks: `number`,
        partitions: `number`
      })
      try {
        if (await db.blocks.count() < 1) throw ''
        this.partitions = await db.partitions.toArray()
        return
      } catch(_) {}
      await db.blocks.bulkPut(new Array(Math.ceil(this.partitions[i].sectors / Disk.BLOCK_SIZE)).fill(0).map((_, number) => ({ number, data: new Uint8Array(Disk.SECTOR_SIZE * Disk.BLOCK_SIZE) })))
      await db.partitions.bulkPut(this.partitions)
      i++
    }
  }

  partition(number) {
    return this.partitions[number]
  }

  async repartition(number, type, sectors) {
    this.partitions[number].type = type
    if (this.partitions[number].sectors != sectors) {
      this.partitions[number].sectors = sectors
      throw 'Resizing partitions is currently not supported, you may only repartition the type'
    }
    await this.dbs[number].partitions.put(this.partitions[number])
  }

  async read(partition, sector, buffer, offset) {
    const block = Math.floor(sector / Disk.BLOCK_SIZE)
    const start = Math.floor((sector - block * Disk.BLOCK_SIZE) * Disk.SECTOR_SIZE)
    const blk = await this.dbs[partition].blocks.get({ number: block })
    for (let i = 0; i < Disk.SECTOR_SIZE; i++) {
      buffer[i + offset] = blk.data[start + i]
    }
  }

  async write(partition, sector, buffer, offset) {
    const block = Math.floor(sector / Disk.BLOCK_SIZE)
    const start = Math.floor((sector - block * Disk.BLOCK_SIZE) * Disk.SECTOR_SIZE)
    const blk = await this.dbs[partition].blocks.get({ number: block })
    for (let i = 0; i < Disk.SECTOR_SIZE; i++) {
      blk.data[start + i] = buffer[i + offset]
    }
    await this.dbs[partition].blocks.put(blk)
  }
}
