import { DoubleLock, readcstr, TrapError, IRQ } from './util.js'

export default class BIOS {
  constructor(ctx, global) {
    this.ctx = ctx
    this.fbufsz = undefined
    this.fbuf = undefined
    this.fbufctx = undefined
    this.global = global
    this.irqtable = {}
    this.irqlock = new DoubleLock(this.ctx.i32a, 0)
  }

  imports() {
    return {
      // Prints a line to the console
      println: (ptr) => {
        console.log(readcstr(this.ctx.init.mem.buffer, ptr))
      },

      // Requests a framebuffer and its size
      framebuffer_getsize: () => {
        this.fbufsz = this.ctx.init.canvas.width * this.ctx.init.canvas.height * 4
        return this.fbufsz
      },

      // Enables a framebuffer at the specified pointer
      framebuffer_set: (ptr) => {
        this.fbuf = new ImageData(new Uint8ClampedArray(this.ctx.init.mem.buffer).subarray(ptr, ptr + this.fbufsz), this.ctx.init.canvas.width, this.ctx.init.canvas.height)
        this.fbufctx = this.ctx.init.canvas.getContext('2d')
      },

      // Blits the framebuffer to the screen. Depending on the thread type and environemnt,
      // it may block for longer or shorter. The implementation requires that the fastest method is used,
      // but buffer-copy-on-message (BCoM) is allowed as a fallback.
      framebuffer_blit: () => {
        this.fbufctx.putImageData(this.fbuf, 0, 0)
        this.fbufctx.commit?.()
      },

      // Sets a JS-defined IRQ handler, '0' will be triggered if none is found
      irq_handler: (irq, func) => {
        this.irqtable[irq] = this.ctx.table.get(func)
        this.global.postMessage({ type: 'irq_enable', irq })
      },

      // Waits for an IRQ
      irq_wait: () => {
        this.irqlock.check(1)
        if (this.ctx.i32a[2] in this.irqtable)
          this.irqtable[this.ctx.i32a[2]]()
        else if (0 in this.irqtable) this.irqtable[0]()
        this.irqlock.validate(1)
      },

      // Stops the WASM execution and transfers into IRQ-only mode.
      // Useful for programs that don't need to do anything other than use IRQs, like the bootROM.
      transfer_to_irq: () => {
        const f = async () => {
          while (true) {
            await this.irqlock.checkAsync(1)
            if (this.ctx.i32a[2] in this.irqtable)
              this.irqtable[this.ctx.i32a[2]]()
            else if (0 in this.irqtable) this.irqtable[0]()
            this.irqlock.validate(1)
          }
        }
        f()
        throw new TrapError('Control transferred to IRQ')
      },

      // Loads partition data
      disk_part: (ptr) => {
        const view = new DataView(this.ctx.init.mem.buffer)
        this.irqlock.lock()
        this.ctx.i32a[2] = IRQ.DISKPART
        this.ctx.i32a[3] = view.getUint8(ptr)
        this.global.postMessage({ type: 'irq' })
        this.irqlock.validate(0)
        this.irqlock.check(0)
        view.setUint8(ptr + 1, this.ctx.i32a[4])
        view.setUint32(ptr + 2, this.ctx.i32a[5], true)
        const ok = this.ctx.i32a[4] >= 0
        this.irqlock.unlock()
        return ok ? 0 : -1
      },

      // Updates partition data
      disk_repart: (ptr) => {
        const view = new DataView(this.ctx.init.mem.buffer)
        this.irqlock.lock()
        this.ctx.i32a[2] = IRQ.DISKREPART
        this.ctx.i32a[3] = view.getUint8(ptr)
        this.ctx.i32a[4] = view.getUint8(ptr + 1)
        this.ctx.i32a[5] = view.getUint32(ptr + 2, true)
        this.global.postMessage({ type: 'irq' })
        this.irqlock.validate(0)
        this.irqlock.check(0)
        this.irqlock.unlock()
      },

      // Read from a disk partition/sector
      disk_read: (par, sec, buf) => {
      },

      // Writr to a disk partition/sector
      disk_write: (par, sec, buf) => {
      }
    }
  }
}
