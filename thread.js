import BIOS from './bios.js'
import { Future, TrapError } from './util.js'

const isInstanceReady = new Future()
const ctx = {}
self.addEventListener('message', async msg => {
  const type = msg.data.type

  if (type == 'init') {
    ctx.init = msg.data

    if (ctx.init.sab) ctx.i32a = new Int32Array(ctx.init.sab)
    if (ctx.init.bios) ctx.init.bios = new BIOS(ctx, self)
    ctx.instance = await WebAssembly.instantiate(ctx.init.mod, {
      env: {
        memory: ctx.init.mem,
        ...ctx.init.bios?.imports()
      }
    })
    if (ctx.instance.exports.memory) ctx.init.mem = ctx.instance.exports.memory
    ctx.table = ctx.instance.exports.__indirect_function_table
    isInstanceReady.now()
  } else if (type == 'start') {
    await isInstanceReady.promise
    try {
      ctx.instance.exports[msg.data.func](...msg.data.args)
    } catch(e) {
      if (!(e instanceof TrapError))
        console.error(e)
    }
  }
})
