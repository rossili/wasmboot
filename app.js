import { loadBootrom, verifyBootrom, Thread, Disk } from './util.js'

const display = document.querySelector('canvas')

// create a disk with one partition of type 0 (unused) and sector count of 64MiB
const disk = new Disk([ { number: 0, type: 0, sectors: 131072 } ])

async function main(opts) {
  await disk.init()
  console.log(disk)
  const bootrom = await loadBootrom()
  if (!opts.fastboot) await verifyBootrom(bootrom)

  const irq = new SharedArrayBuffer(64 * 1024)
  const thread = new Thread(bootrom, { bios: true, sab: irq, canvas: display.transferControlToOffscreen(), disk })
  thread.start()
}

function resize() {
  const w = window.innerWidth
  const h = window.innerHeight
  const dw = display.width
  const dh = display.height
  const dar = dw / dh
  if (h * dar > w) {
    display.style.width = `${w}px`
    display.style.height = `${w / dar}px`
  } else {
    display.style.width = `${h * dar}px`
    display.style.height = `${h}px`
  }
}
window.addEventListener('resize', resize)

const opt = { fastboot: false }
{
  const params = new URLSearchParams(location.search)
  if (params.get('fastboot') !== null && params.get('fastboot') !== 'unsafe') {
    params.delete('fastboot')
    if (confirm('Fastboot detected! This may allow unsigned software to run without checks. Do you want to keep fastboot?')) {
      params.set('fastboot', 'unsafe')
    }
    location.search = params.toString()
  }

  if (params.get('fastboot') === 'unsafe') opt.fastboot = true
  const w = params.get('w')
  const h = params.get('h')
  if (w) display.width = parseInt(w)
  if (h) display.height = parseInt(h)
}

resize()
main(opt)
